package db;
 
public class Zamowienie {
	private int NrZamowienia;
	private int Ilosc;
	private String DataZamowienia;
    private int FkIdKlient;
    private int FkIdProdukt;
 
    public void setNrZamowienia(int NrZamowienia) {
    	this.NrZamowienia = NrZamowienia;
    }
    public int getIlosc() {
    	return Ilosc;
    }
    public void setIlosc(int Ilosc) {
    	this.Ilosc = Ilosc;
    }
    public String getDataZamowienia() {
    	return DataZamowienia;
    }
    public void setDataZamowienia(String DataZamowienia) {
    	this.DataZamowienia = DataZamowienia;
    }
    public int getFkIdKlient() {
    	return FkIdKlient;
    }
    public void setFkIdKlient(int FkIdKlient) {
    	this.FkIdKlient = FkIdKlient;
    }
    public int getFkIdProdukt() {
    	return FkIdProdukt;
    }
    public void setFkIdProdukt(int FkIdProdukt) {
    	this.FkIdProdukt = FkIdProdukt;
    }
    public Zamowienie(int NrZamowienia,  int Ilosc, String DataZamowienia, int FkIdKlient, int FkIdProdukt) {
        this.NrZamowienia = NrZamowienia;
        this.Ilosc = Ilosc;
        this.DataZamowienia = DataZamowienia;
        this.FkIdKlient = FkIdKlient;
        this.FkIdProdukt = FkIdProdukt;
    }
    
    public String toString() {
        return NrZamowienia + " | " + Ilosc + " | " + DataZamowienia + " | " + FkIdKlient + " | " + FkIdProdukt;
    }
}