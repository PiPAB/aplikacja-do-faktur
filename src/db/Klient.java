package db;

public class Klient {

    private int idKlient;
    private String NazwaFirmy;
    private String NIP;
    private String Imie;
    private String Nazwisko;
    private String Adres;
    private String KodPocztowy;
    private String Miejscowosc;
    private String Telefon;
    private String Email;
 
    public int getIdKlient() {
        return idKlient;
    }
    public String getNazwaFirmy() {
        return NazwaFirmy;
    }
    public void setNazwaFirmy(String NazwaFirmy) {
        this.NazwaFirmy = NazwaFirmy;
    }
    public String getNIP() {
        return NIP;
    }
    public void setNIP(String NIP) {
        this.NIP = NIP;
    }
    public String getImie() {
        return Imie;
    }
    public void setImie(String Imie) {
        this.Imie = Imie;
    }
    public String getNazwisko() {
    	return Nazwisko;
    }
    public void setNazwisko(String Nazwisko) {
    	this.Nazwisko = Nazwisko;
    }
    public String getAdres() {
    	return Adres;
    }
    public void setAdres(String Adres) {
    	this.Adres = Adres;
    }
    public String getKodPocztowy() {
    	return KodPocztowy;
    }
    public void setKodPocztowy(String KodPocztowy) {
    	this.KodPocztowy = KodPocztowy;
    }
    public String getMiejscowosc() {
    	return Miejscowosc;
    }
    public void setMiejscowosc(String Miejscowosc) {
    	this.Miejscowosc = Miejscowosc;
    }
    public String GetTelefon() {
    	return Telefon;
    }
    public void setTelefon(String Telefon){
    	this.Telefon = Telefon;
    }
   public String getEmail() {
	   return Email;
   }
    public void setEmail(String Email) {
    	this.Email = Email;
    }
    
    public Klient(int idKlient, String NazwaFirmy, String NIP, String Imie, String Nazwisko, String Adres, String KodPocztowy, String Miejscowosc, String Telefon, String Email) {
        this.idKlient = idKlient;
        this.NazwaFirmy = NazwaFirmy;
        this.NIP = NIP;
        this.Imie = Imie;
        this.Nazwisko = Nazwisko;
        this.Adres = Adres;
        this.KodPocztowy = KodPocztowy;
        this.Miejscowosc = Miejscowosc;
        this.Telefon = Telefon;
        this.Email = Email;
    }
    public String toString() {
        return idKlient+" | " + NazwaFirmy + " | " + NIP + " | " + Imie + " | " + Nazwisko + " | " 
        	+ Adres + " | " + KodPocztowy + " | " + Miejscowosc + " | " + Telefon + " | " + Email ;
    }
}