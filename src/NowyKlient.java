import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

public class NowyKlient extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField textFieldImie;
	private JTextField textFieldNazwisko;
	private JTextField textFieldNazwaFirmy;
	private JTextField textFieldAdres;
	private JTextField textFieldMiejscowosc;
	private JTextField textFieldKod_pocztowy;
	private JTextField textFieldNIP;
	private JTextField textFieldTelefon;
	private JTextField textFieldE_mail;
	private JLabel labelError = new JLabel("");

	/**
	 * Launch the application.
	 */
	public static void NewClientWindow() {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					NowyKlient nowyKlient = new NowyKlient();
					nowyKlient.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public NowyKlient() {
		setResizable(false);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 450, 550);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		
		JLabel labelImie = new JLabel("Imi\u0119");
		labelImie.setFont(new Font("Arial", Font.PLAIN, 15));
		
		JLabel labelNowyKlient = new JLabel("Nowy Klient");
		labelNowyKlient.setFont(new Font("Arial", Font.BOLD, 20));
		labelNowyKlient.setHorizontalAlignment(SwingConstants.CENTER);
		
		JLabel labelNazwisko = new JLabel("Nazwisko");
		labelNazwisko.setFont(new Font("Arial", Font.PLAIN, 15));
		
		JLabel labelNazwaFirmy = new JLabel("Nazwa firmy*");
		labelNazwaFirmy.setFont(new Font("Arial", Font.PLAIN, 15));
		
		JLabel labelAdres = new JLabel("Adres*");
		labelAdres.setFont(new Font("Arial", Font.PLAIN, 15));
		
		JLabel labelMiejscowosc = new JLabel("Miejscowo\u015B\u0107*");
		labelMiejscowosc.setFont(new Font("Arial", Font.PLAIN, 15));
		
		JLabel labelKod_poczotwy = new JLabel("Kod pocztowy*");
		labelKod_poczotwy.setFont(new Font("Arial", Font.PLAIN, 15));
		
		JLabel labelNIP = new JLabel("NIP*");
		labelNIP.setFont(new Font("Arial", Font.PLAIN, 15));
		
		JLabel labelTelefon = new JLabel("Telefon");
		labelTelefon.setFont(new Font("Arial", Font.PLAIN, 15));
		
		JLabel labelE_mail = new JLabel("E-mail*");
		labelE_mail.setFont(new Font("Arial", Font.PLAIN, 15));
		
		textFieldImie = new JTextField();
		textFieldImie.setColumns(10);
		
		textFieldNazwisko = new JTextField();
		textFieldNazwisko.setColumns(10);
		
		textFieldNazwaFirmy = new JTextField();
		textFieldNazwaFirmy.setColumns(10);
		
		textFieldAdres = new JTextField();
		textFieldAdres.setColumns(10);
		
		textFieldMiejscowosc = new JTextField();
		textFieldMiejscowosc.setColumns(10);
		
		textFieldKod_pocztowy = new JTextField();
		textFieldKod_pocztowy.setColumns(10);
		
		textFieldNIP = new JTextField();
		textFieldNIP.setColumns(10);
		
		textFieldTelefon = new JTextField();
		textFieldTelefon.setColumns(10);
		
		textFieldE_mail = new JTextField();
		textFieldE_mail.setColumns(10);
		
		JButton buttonPotwierdz = new JButton("Zapisz");
		buttonPotwierdz.addActionListener(new ActionListener() {//przycisk dodawania nowych klient�w do bazy
			public void actionPerformed(ActionEvent arg0) {
				boolean Sprawdzenie = true; //je�eli ta zmienna pozostaje true, to wszystko jest w porz�dku
				String NIP = textFieldNIP.getText(); //zczytywanie z odpowidnich p�l tekstowych do zmiennych
				String Imie = textFieldImie.getText();
				String Nazwisko = textFieldNazwisko.getText();
				String Telefon = textFieldTelefon.getText();
				String Email = textFieldE_mail.getText();
				if (!Email.contains("@")){ //adres e-mail musi zawiera� @
					Sprawdzenie = false;
					labelError.setText("Nieprawid�owy E-Mail");
				}
				if (NIP.isEmpty()){ //if sprawdzaj� pola obowi�zkowe
					Sprawdzenie = false;
					labelError.setText("NIP jest wymagany.");
				}
				String KodPocztowy = textFieldKod_pocztowy.getText();
				if (KodPocztowy.isEmpty()){
					Sprawdzenie = false;
					labelError.setText("Kod pocztowy jest wymagany.");
				}
				String Miejscowosc = textFieldMiejscowosc.getText();
				if (Miejscowosc.isEmpty()){
					Sprawdzenie = false;
					labelError.setText("Miejscowo�� jest wymagana.");
				}
				String Adres = textFieldAdres.getText();
				if (Adres.isEmpty()){
					Sprawdzenie = false;
					labelError.setText("Adres jest wymagany.");
				}
				String NazwaFirmy = textFieldNazwaFirmy.getText();
				if (NazwaFirmy.isEmpty()){
					Sprawdzenie = false;
					labelError.setText("Nazwa firmy jest wymagana.");
				}
				if (Sprawdzenie) //je�eli gdzie� powy�ej jest puste pole lub z�y mail, to zapytanie nie zostanie wykonane
					try { //pr�ba zapytania
						Menu.baza.insertKlient(NazwaFirmy, NIP, Imie, Nazwisko, Adres, KodPocztowy, Miejscowosc, Telefon, Email);
						labelError.setText("Klient dodany!");
						MenuKlient.zaczytajDane();
						dispose();
					} catch (Exception e) {
						labelError.setText("Nieprawid�owe dane wej�ciowe!");
					}
			}
		});
		buttonPotwierdz.setFont(new Font("Tahoma", Font.BOLD, 11));
		
		JButton buttonAnuluj = new JButton("Anuluj");
		buttonAnuluj.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		buttonAnuluj.setFont(new Font("Tahoma", Font.PLAIN, 11));
		
		JLabel lblpoleObowizkowe = new JLabel("*pole obowi\u0105zkowe");
		lblpoleObowizkowe.setForeground(Color.RED);
		
		
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_contentPane.createSequentialGroup()
							.addComponent(buttonPotwierdz, GroupLayout.PREFERRED_SIZE, 128, GroupLayout.PREFERRED_SIZE)
							.addContainerGap())
						.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
							.addGroup(gl_contentPane.createSequentialGroup()
								.addComponent(labelNazwisko, GroupLayout.PREFERRED_SIZE, 76, GroupLayout.PREFERRED_SIZE)
								.addContainerGap(348, Short.MAX_VALUE))
							.addGroup(gl_contentPane.createSequentialGroup()
								.addComponent(labelNazwaFirmy, GroupLayout.PREFERRED_SIZE, 94, GroupLayout.PREFERRED_SIZE)
								.addContainerGap(330, Short.MAX_VALUE))
							.addGroup(gl_contentPane.createSequentialGroup()
								.addComponent(labelAdres, GroupLayout.PREFERRED_SIZE, 94, GroupLayout.PREFERRED_SIZE)
								.addContainerGap(330, Short.MAX_VALUE))
							.addGroup(gl_contentPane.createSequentialGroup()
								.addComponent(labelMiejscowosc, GroupLayout.PREFERRED_SIZE, 94, GroupLayout.PREFERRED_SIZE)
								.addContainerGap(330, Short.MAX_VALUE))
							.addGroup(gl_contentPane.createSequentialGroup()
								.addComponent(labelNIP, GroupLayout.PREFERRED_SIZE, 94, GroupLayout.PREFERRED_SIZE)
								.addContainerGap(330, Short.MAX_VALUE))
							.addGroup(gl_contentPane.createSequentialGroup()
								.addComponent(labelTelefon, GroupLayout.PREFERRED_SIZE, 94, GroupLayout.PREFERRED_SIZE)
								.addContainerGap(330, Short.MAX_VALUE))
							.addGroup(gl_contentPane.createSequentialGroup()
								.addComponent(labelE_mail, GroupLayout.PREFERRED_SIZE, 94, GroupLayout.PREFERRED_SIZE)
								.addContainerGap(330, Short.MAX_VALUE))
							.addGroup(gl_contentPane.createSequentialGroup()
								.addGroup(gl_contentPane.createParallelGroup(Alignment.TRAILING)
									.addComponent(labelNowyKlient, GroupLayout.DEFAULT_SIZE, 411, Short.MAX_VALUE)
									.addGroup(gl_contentPane.createSequentialGroup()
										.addComponent(labelImie, GroupLayout.PREFERRED_SIZE, 66, GroupLayout.PREFERRED_SIZE)
										.addPreferredGap(ComponentPlacement.RELATED, 65, Short.MAX_VALUE)
										.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
											.addComponent(textFieldNazwisko, GroupLayout.PREFERRED_SIZE, 280, GroupLayout.PREFERRED_SIZE)
											.addComponent(textFieldImie, GroupLayout.PREFERRED_SIZE, 280, GroupLayout.PREFERRED_SIZE)
											.addComponent(textFieldNazwaFirmy, GroupLayout.PREFERRED_SIZE, 280, GroupLayout.PREFERRED_SIZE)
											.addComponent(textFieldAdres, GroupLayout.PREFERRED_SIZE, 280, GroupLayout.PREFERRED_SIZE)
											.addComponent(textFieldMiejscowosc, GroupLayout.PREFERRED_SIZE, 280, GroupLayout.PREFERRED_SIZE)
											.addComponent(textFieldKod_pocztowy, GroupLayout.PREFERRED_SIZE, 280, GroupLayout.PREFERRED_SIZE)
											.addComponent(textFieldNIP, GroupLayout.PREFERRED_SIZE, 280, GroupLayout.PREFERRED_SIZE)
											.addComponent(textFieldTelefon, GroupLayout.PREFERRED_SIZE, 280, GroupLayout.PREFERRED_SIZE)
											.addComponent(textFieldE_mail, GroupLayout.PREFERRED_SIZE, 280, GroupLayout.PREFERRED_SIZE)
											.addComponent(buttonAnuluj, Alignment.TRAILING, GroupLayout.PREFERRED_SIZE, 128, GroupLayout.PREFERRED_SIZE)
											.addComponent(lblpoleObowizkowe)
											.addComponent(labelError))))
								.addGap(13))
							.addGroup(gl_contentPane.createSequentialGroup()
								.addComponent(labelKod_poczotwy, GroupLayout.PREFERRED_SIZE, 111, GroupLayout.PREFERRED_SIZE)
								.addContainerGap()))))
		);
		gl_contentPane.setVerticalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addContainerGap()
					.addComponent(labelNowyKlient)
					.addGap(22)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(labelImie)
						.addComponent(textFieldImie, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(18)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(labelNazwisko, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
						.addComponent(textFieldNazwisko, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(18)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(labelNazwaFirmy, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
						.addComponent(textFieldNazwaFirmy, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(18)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(labelAdres, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
						.addComponent(textFieldAdres, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(18)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(labelMiejscowosc, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
						.addComponent(textFieldMiejscowosc, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(18)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(labelKod_poczotwy, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
						.addComponent(textFieldKod_pocztowy, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(18)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(labelNIP, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
						.addComponent(textFieldNIP, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(18)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(labelTelefon, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
						.addComponent(textFieldTelefon, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(18)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(labelE_mail, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
						.addComponent(textFieldE_mail, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(lblpoleObowizkowe)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(labelError)
					.addPreferredGap(ComponentPlacement.RELATED, 13, Short.MAX_VALUE)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addComponent(buttonPotwierdz, GroupLayout.PREFERRED_SIZE, 31, GroupLayout.PREFERRED_SIZE)
						.addComponent(buttonAnuluj, GroupLayout.PREFERRED_SIZE, 31, GroupLayout.PREFERRED_SIZE))
					.addContainerGap())
		);
		contentPane.setLayout(gl_contentPane);
	}
}
