package db;
 
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;
 
import db.Klient;
import db.Produkt;
import db.Zamowienie;
 
public class Faktura {
 
    public static final String DRIVER = "org.sqlite.JDBC";
    public static final String DB_URL = "jdbc:sqlite:faktura.db";
 
    private Connection conn;
    private Statement stat;
 
    public Faktura() {
        try {
            Class.forName(Faktura.DRIVER);
        } catch (ClassNotFoundException e) {
            System.err.println("Brak sterownika JDBC");
            e.printStackTrace();
        }
 
        try {
            conn = DriverManager.getConnection(DB_URL);
            stat = conn.createStatement();
        } catch (SQLException e) {
            System.err.println("Problem z otwarciem polaczenia");
            e.printStackTrace();
        }
 
        createTables();
    }
 
    // Tworzenie tabel 
    public boolean createTables()  {
    	String createProdukty = "CREATE TABLE IF NOT EXISTS "
        		+ "Produkty ("
        		+ "idProdukt INTEGER PRIMARY KEY AUTOINCREMENT, "
        		+ "NazwaProduktu varchar(255), "
        		+ "CenaNetto real(16),"
        		+ "Vat int,"
        		+ "CenaBrutto real(16))";
        String createKlienci = "CREATE TABLE IF NOT EXISTS "
        		+ "Klienci ("
        		+ "idKlient INTEGER PRIMARY KEY AUTOINCREMENT, "
        		+ "NazwaFirmy varchar(255), "
        		+ "NIP varchar(255),"
        		+ "Imie varchar(255),"
        		+ "Nazwisko varchar(255),"
        		+ "Adres varchar(255),"
        		+ "KodPocztowy varchar(255),"
        		+ "Miejscowosc varchar(255),"
        		+ "Telefon varchar(255),"
        		+ "Email varchar(255))";
        String createZamowienia = "CREATE TABLE IF NOT EXISTS "
        		+ "Zamowienia ("
        		+ "NrZamowienia int, "
        		+ "Ilosc int, "
        		+ "DataZamowienia varchar(255), "
        		+ "FkIdKlient int, "
        		+ "FkIdProdukt int, "
        		+ "FOREIGN KEY(FkIdKlient) REFERENCES Klienci(idKlient),"
        		+ "FOREIGN KEY(FkIdProdukt) REFERENCES Produkty(idProdukt))"
        		;
        try {
            stat.execute(createKlienci);
            stat.execute(createProdukty);
            stat.execute(createZamowienia);
        } catch (SQLException e) {
            System.err.println("Blad przy tworzeniu tabeli");
            e.printStackTrace();
            return false;
        }
        return true;
    }
 
    // wstawienie klienta do bazy
    public boolean insertKlient(String NazwaFirmy, String NIP, String Imie, String Nazwisko, 
    		String Adres, String KodPocztowy, String Miejscowosc, String Telefon, String Email) {
        try {
            PreparedStatement prepStmt = conn.prepareStatement(
                    "INSERT INTO Klienci VALUES (NULL, ?, ?, ?, ?, ?, ?, ?, ?, ?);");
            prepStmt.setString(1, NazwaFirmy);
            prepStmt.setString(2, NIP);
            prepStmt.setString(3, Imie);
            prepStmt.setString(4, Nazwisko);
            prepStmt.setString(5, Adres);
            prepStmt.setString(6, KodPocztowy);
            prepStmt.setString(7, Miejscowosc);
            prepStmt.setString(8, Telefon);
            prepStmt.setString(9, Email);
            prepStmt.execute();
        } catch (SQLException e) {
            System.err.println("Blad przy wstawianiu klienta do bazy");
            e.printStackTrace();
            return false;
        }
        return true;
    }
    
    // usuniecie klienta o nr idKlient z bazy
    public boolean deleteKlient(int idKlient)  {
    	String deleteKlient = "DELETE FROM Klienci WHERE idKlient = "+idKlient+";)";
        try {
            stat.execute(deleteKlient);
        } catch (SQLException e) {
            System.err.println("Blad usuniecia klienta z bazy");
            e.printStackTrace();
            return false;
        }
        return true;
    }
    
    // edytuj klienta o nr idKlient
    public boolean updateKlient(int idKlient, String NazwaFirmy, String NIP, String Imie, String Nazwisko, 
    	String Adres, String KodPocztowy, String Miejscowosc, String Telefon, String Email) {
    	String updateKlient = "UPDATE Klienci SET "
    			+ "NazwaFirmy='"+NazwaFirmy+"', "
    			+ "NIP='"+NIP+"', "
    			+ "Imie='"+Imie+"', "
    			+ "Nazwisko='"+Nazwisko+"', "
    			+ "Adres='"+Adres+"', "
    			+ "KodPocztowy='"+KodPocztowy+"', "
    			+ "Miejscowosc='"+Miejscowosc+"', "
    			+ "Telefon='"+Telefon+"', "
    			+ "Email='"+Email+"'"
    			+ " WHERE idKlient = "+idKlient+";)";
    	try {
            stat.execute(updateKlient);
        } catch (SQLException e) {
            System.err.println("Blad edycji klienta");
            e.printStackTrace();
            return false;
        }
        return true;
    }
    
    // wstawienie produktu do bazy 
    public boolean insertProdukt(String NazwaProduktu, float CenaNetto, int Vat, float CenaBrutto) {
        try {
            PreparedStatement prepStmt = conn.prepareStatement(
                    "INSERT INTO Produkty VALUES (NULL, ?, ?, ?, ?);");
            prepStmt.setString(1, NazwaProduktu);
            prepStmt.setFloat(2, CenaNetto);
            prepStmt.setInt(3, Vat);
            prepStmt.setFloat(4, CenaBrutto);
            prepStmt.execute();
        } catch (SQLException e) {
            System.err.println("Blad przy wstawianiu produktu do bazy");
            return false;
        }
        return true;
    }
    
 // usuniecie produktu o nr idProdukt z bazy
    public boolean deleteProdukt(int idProdukt)  {
    	String deleteProdukt = "DELETE FROM Produkty WHERE idProdukt = "+idProdukt+";)";
        try {
            stat.execute(deleteProdukt);
        } catch (SQLException e) {
            System.err.println("Blad usuniecia produktu");
            e.printStackTrace();
            return false;
        }
        return true;
    }
    
    // edytuj produkt w bazie
    public boolean updateProdukt(int idProdukt, String NazwaProduktu, float CenaNetto, int Vat, float CenaBrutto) {
    	String updateKlient = "UPDATE Produkty SET "
    			+ "NazwaProduktu='"+NazwaProduktu+"', "
    			+ "CenaNetto='"+CenaNetto+"', "
    			+ "Vat='"+Vat+"', "
    			+ "CenaBrutto='"+CenaBrutto+"'"
    			+ " WHERE idProdukt = "+idProdukt+");";
    	try {
            stat.execute(updateKlient);
        } catch (SQLException e) {
            System.err.println("Blad edycji produktu");
            e.printStackTrace();
            return false;
        }
        return true;
    }
    
    // wstawienie zamowienia do bazy    
    public boolean insertZamowienie(int NrZamowienia, int Ilosc, String DataZamowienia, int FkIdKlient, int FkIdProdukt) {
        try {
            PreparedStatement prepStmt = conn.prepareStatement(
                    "INSERT INTO Zamowienia VALUES (?, ?, ?, ?, ?);");
            prepStmt.setInt(1, NrZamowienia);
            prepStmt.setInt(2, Ilosc);
            prepStmt.setString(3, DataZamowienia);
            prepStmt.setInt(4, FkIdKlient);
            prepStmt.setInt(5, FkIdProdukt);
            prepStmt.execute();
        } catch (SQLException e) {
            System.err.println("Blad przy wstawienu zamowienia");
            return false;
        }
        return true;
    }
   
    // usuniecie zamowienia o numerze NrZamowienia
    public boolean deleteZamowienie(int NrZamowienia) {
    	String deleteZamowienie = "DELETE FROM Zamowienia WHERE NrZamowienia = "+NrZamowienia+";)";
    	try {
    		stat.execute(deleteZamowienie);
    	} catch (SQLException e) {
    		System.err.println("Blad usuniecia zamowienia");
    		e.printStackTrace();
    		return false;
    	}
    	return false;
    }
    
    // edycja zamowienia
    public boolean updateZamowienie(int NrZamowienia, int Ilosc, 
    	String DataZamowienia, int FkIdKlient, int FkIdProdukt) {
    	String updateZamowienie = "UPDATE Zamowienie SET "
    			+ "Ilosc="+Ilosc+", "
    			+ "DataZamowienia='"+DataZamowienia+"', "
    			+ "FkIdKlient="+FkIdKlient+", "
    			+ "FkIdProdukt="+FkIdProdukt+" "
    			+ " WHERE NrZamowienia = "+NrZamowienia+");";
    	try {
            stat.execute(updateZamowienie);
        } catch (SQLException e) {
            System.err.println("Blad aktualizacji zamowienia");
            e.printStackTrace();
            
            return false;
        }
        return true;
    }
    
    // pokaz klientow
    public List<Klient> selectKlienci() {
        List<Klient> klienci = new LinkedList<Klient>();
        try {
            ResultSet result = stat.executeQuery("SELECT * FROM Klienci");
            int id;
            String NazwaFirmy, NIP, Imie, Nazwisko, Adres, KodPocztowy, Miejscowosc, Telefon, Email;
            
            while(result.next()) {
                id = result.getInt("idKlient");
                NazwaFirmy = result.getString("NazwaFirmy");
                NIP = result.getString("NIP");
                Imie = result.getString("Imie");
                Nazwisko = result.getString("Nazwisko");
                Adres = result.getString("Adres");
                KodPocztowy = result.getString("KodPocztowy");
                Miejscowosc = result.getString("Miejscowosc");
                Telefon = result.getString("Telefon");
                Email = result.getString("Email");
                
                klienci.add(new Klient(id, NazwaFirmy, NIP, Imie, Nazwisko, Adres, KodPocztowy, Miejscowosc, Telefon, Email));
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
        return klienci;
    }

    // pokaz produkty
    public List<Produkt> selectProdukty() {
        List<Produkt> produkty = new LinkedList<Produkt>();
        try {
            ResultSet result = stat.executeQuery("SELECT * FROM Produkty");
            int id, Vat;
			float CenaNetto, CenaBrutto;        
            String NazwaProduktu;

            while(result.next()) {
                id = result.getInt("idProdukt");
                NazwaProduktu = result.getString("NazwaProduktu");
                CenaNetto = result.getFloat("CenaNetto");
                Vat = result.getInt("Vat");
                CenaBrutto = result.getFloat("CenaBrutto");

                produkty.add(new Produkt(id, NazwaProduktu, CenaNetto, Vat, CenaBrutto));
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
        return produkty;
    }
 
    // pokaz zamowienia
    public List<Zamowienie> selectZamowienia() {
        List<Zamowienie> zamowienia = new LinkedList<Zamowienie>();
        try {
            ResultSet result = stat.executeQuery("SELECT * FROM Zamowienia");

            int NrZamowienia, Ilosc;
            String DataZamowienia; 
            int FkIdKlient, FkIdProdukt;

            while(result.next()) {
                NrZamowienia = result.getInt("NrZamowienia");
                Ilosc = result.getInt("Ilosc");
                DataZamowienia = result.getString("DataZamowienia");
                FkIdKlient = result.getInt("FkIdKlient");
                FkIdProdukt = result.getInt("FkIdProdukt");

                zamowienia.add(new Zamowienie(NrZamowienia, Ilosc, DataZamowienia, FkIdKlient, FkIdProdukt));
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
        return zamowienia;
    }
   
    public void closeConnection() {
        try {
            conn.close();
        } catch (SQLException e) {
            System.err.println("Problem z zamknieciem polaczenia");
            e.printStackTrace();
        }
    }
}