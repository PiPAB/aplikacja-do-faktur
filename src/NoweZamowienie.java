import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

public class NoweZamowienie extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField textFieldIlosc;
	private JTextField textFieldCenaNetto;
	private JTextField textFieldCenaBrutto;

	/**
	 * Launch the application.
	 */
	public static void NewOrderWindow() {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					NoweZamowienie frame = new NoweZamowienie();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public NoweZamowienie() {
		setResizable(false);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 450, 350);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		
		JLabel lblNoweZamwienie = new JLabel("Nowe Zam\u00F3wienie");
		lblNoweZamwienie.setHorizontalAlignment(SwingConstants.CENTER);
		lblNoweZamwienie.setFont(new Font("Arial", Font.BOLD, 20));
		
		JLabel lblKlient = new JLabel("Klient*");
		lblKlient.setFont(new Font("Arial", Font.PLAIN, 15));
		
		JLabel lblProdukt = new JLabel("Produkt*");
		lblProdukt.setFont(new Font("Arial", Font.PLAIN, 15));
		
		JLabel lblIlo = new JLabel("Ilo\u015B\u0107*");
		lblIlo.setFont(new Font("Arial", Font.PLAIN, 15));
		
		JLabel lblCenaNetto = new JLabel("Cena Netto");
		lblCenaNetto.setFont(new Font("Arial", Font.PLAIN, 15));
		
		JLabel lblCenaBrutto = new JLabel("Cena Brutto");
		lblCenaBrutto.setFont(new Font("Arial", Font.PLAIN, 15));
		
		textFieldIlosc = new JTextField();
		textFieldIlosc.setColumns(10);
		
		JComboBox<String> comboBoxProdukt = new JComboBox<String>();
		comboBoxProdukt.setMaximumRowCount(1024);
		comboBoxProdukt.setModel(new DefaultComboBoxModel<String>(new String[] {"Wybierz produkt"}));
		
		JComboBox<String> comboBoxKlient = new JComboBox<String>();
		comboBoxKlient.setModel(new DefaultComboBoxModel<String>(new String[] {"Wybierz klienta"}));
		comboBoxKlient.setToolTipText("");
		comboBoxKlient.setMaximumRowCount(1024);
		
		textFieldCenaNetto = new JTextField();
		textFieldCenaNetto.setEditable(false);
		textFieldCenaNetto.setColumns(10);
		
		textFieldCenaBrutto = new JTextField();
		textFieldCenaBrutto.setEditable(false);
		textFieldCenaBrutto.setColumns(10);
		
		JLabel label = new JLabel("*pole obowi\u0105zkowe");
		label.setForeground(Color.RED);
		
		JButton btnAnuluj = new JButton("Anuluj");
		btnAnuluj.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			dispose();}
		});
		btnAnuluj.setFont(new Font("Tahoma", Font.PLAIN, 11));
		
		JButton btnZapisz = new JButton("Zapisz");
		btnZapisz.setFont(new Font("Tahoma", Font.BOLD, 11));
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_contentPane.createParallelGroup(Alignment.TRAILING)
						.addComponent(lblNoweZamwienie, GroupLayout.PREFERRED_SIZE, 411, GroupLayout.PREFERRED_SIZE)
						.addGroup(gl_contentPane.createSequentialGroup()
							.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
								.addComponent(lblIlo, GroupLayout.PREFERRED_SIZE, 94, GroupLayout.PREFERRED_SIZE)
								.addComponent(lblProdukt, GroupLayout.PREFERRED_SIZE, 94, GroupLayout.PREFERRED_SIZE)
								.addComponent(lblKlient, GroupLayout.PREFERRED_SIZE, 94, GroupLayout.PREFERRED_SIZE)
								.addComponent(lblCenaNetto, GroupLayout.PREFERRED_SIZE, 94, GroupLayout.PREFERRED_SIZE)
								.addComponent(lblCenaBrutto, GroupLayout.PREFERRED_SIZE, 94, GroupLayout.PREFERRED_SIZE))
							.addGap(37)
							.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING, false)
								.addComponent(textFieldIlosc, GroupLayout.DEFAULT_SIZE, 291, Short.MAX_VALUE)
								.addComponent(comboBoxProdukt, 0, 291, Short.MAX_VALUE)
								.addComponent(comboBoxKlient, 0, 291, Short.MAX_VALUE)
								.addComponent(label, GroupLayout.PREFERRED_SIZE, 129, GroupLayout.PREFERRED_SIZE)
								.addComponent(textFieldCenaBrutto)
								.addComponent(textFieldCenaNetto, Alignment.TRAILING, GroupLayout.PREFERRED_SIZE, 291, GroupLayout.PREFERRED_SIZE)))
						.addGroup(gl_contentPane.createSequentialGroup()
							.addComponent(btnZapisz, GroupLayout.PREFERRED_SIZE, 128, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.RELATED, 158, Short.MAX_VALUE)
							.addComponent(btnAnuluj, GroupLayout.PREFERRED_SIZE, 128, GroupLayout.PREFERRED_SIZE)))
					.addContainerGap())
		);
		gl_contentPane.setVerticalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addContainerGap()
					.addComponent(lblNoweZamwienie, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE)
					.addGap(18)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblKlient, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
						.addComponent(comboBoxKlient, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(18)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblProdukt, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
						.addComponent(comboBoxProdukt, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(18)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblIlo, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
						.addComponent(textFieldIlosc, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(18)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblCenaNetto, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
						.addComponent(textFieldCenaNetto, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(16)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblCenaBrutto, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
						.addComponent(textFieldCenaBrutto, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(18)
					.addComponent(label)
					.addPreferredGap(ComponentPlacement.RELATED, 26, Short.MAX_VALUE)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addComponent(btnAnuluj, GroupLayout.PREFERRED_SIZE, 31, GroupLayout.PREFERRED_SIZE)
						.addComponent(btnZapisz, GroupLayout.PREFERRED_SIZE, 31, GroupLayout.PREFERRED_SIZE))
					.addContainerGap())
		);
		contentPane.setLayout(gl_contentPane);
	}
}
