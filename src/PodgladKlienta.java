import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

public class PodgladKlienta extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField textFieldImie;
	private JTextField textFieldNazwisko;
	private JTextField textFieldNazwaFirmy;
	private JTextField textFieldAdres;
	private JTextField textFieldMiejscowosc;
	private JTextField textFieldKod_pocztowy;
	private JTextField textFieldNIP;
	private JTextField textFieldTelefon;
	private JTextField textFieldE_mail;

	/**
	 * Launch the application.
	 */
	public static void ShowClientWindow() {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					PodgladKlienta podgladKlienta = new PodgladKlienta();
					podgladKlienta.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public PodgladKlienta() {
		setResizable(false);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 450, 520);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		
		
		JLabel labelImie = new JLabel("Imi\u0119");
		labelImie.setFont(new Font("Arial", Font.PLAIN, 15));
		
		JLabel labelEdycjaKlienta = new JLabel("Podgl\u0105d Klienta");
		labelEdycjaKlienta.setFont(new Font("Arial", Font.BOLD, 20));
		labelEdycjaKlienta.setHorizontalAlignment(SwingConstants.CENTER);
		
		JLabel labelNazwisko = new JLabel("Nazwisko");
		labelNazwisko.setFont(new Font("Arial", Font.PLAIN, 15));
		
		JLabel labelNazwaFirmy = new JLabel("Nazwa firmy*");
		labelNazwaFirmy.setFont(new Font("Arial", Font.PLAIN, 15));
		
		JLabel labelAdres = new JLabel("Adres*");
		labelAdres.setFont(new Font("Arial", Font.PLAIN, 15));
		
		JLabel labelMiejscowosc = new JLabel("Miejscowo\u015B\u0107*");
		labelMiejscowosc.setFont(new Font("Arial", Font.PLAIN, 15));
		
		JLabel labelKod_poczotwy = new JLabel("Kod pocztowy*");
		labelKod_poczotwy.setFont(new Font("Arial", Font.PLAIN, 15));
		
		JLabel labelNIP = new JLabel("NIP*");
		labelNIP.setFont(new Font("Arial", Font.PLAIN, 15));
		
		JLabel labelTelefon = new JLabel("Telefon");
		labelTelefon.setFont(new Font("Arial", Font.PLAIN, 15));
		
		JLabel labelE_mail = new JLabel("E-mail*");
		labelE_mail.setFont(new Font("Arial", Font.PLAIN, 15));
		
		textFieldImie = new JTextField();
		textFieldImie.setEditable(false);
		textFieldImie.setColumns(10);
		
		textFieldNazwisko = new JTextField();
		textFieldNazwisko.setEditable(false);
		textFieldNazwisko.setColumns(10);
		
		textFieldNazwaFirmy = new JTextField();
		textFieldNazwaFirmy.setEditable(false);
		textFieldNazwaFirmy.setColumns(10);
		
		textFieldAdres = new JTextField();
		textFieldAdres.setEditable(false);
		textFieldAdres.setColumns(10);
		
		textFieldMiejscowosc = new JTextField();
		textFieldMiejscowosc.setEditable(false);
		textFieldMiejscowosc.setColumns(10);
		
		textFieldKod_pocztowy = new JTextField();
		textFieldKod_pocztowy.setEditable(false);
		textFieldKod_pocztowy.setColumns(10);
		
		textFieldNIP = new JTextField();
		textFieldNIP.setEditable(false);
		textFieldNIP.setColumns(10);
		
		textFieldTelefon = new JTextField();
		textFieldTelefon.setEditable(false);
		textFieldTelefon.setColumns(10);
		
		textFieldE_mail = new JTextField();
		textFieldE_mail.setEditable(false);
		textFieldE_mail.setColumns(10);
		
		JButton buttonPotwierdz = new JButton("Zamknij");
		buttonPotwierdz.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				dispose();
			}
		});
		buttonPotwierdz.setFont(new Font("Tahoma", Font.BOLD, 11));
		
		JLabel lblpoleObowizkowe = new JLabel("*pole obowi\u0105zkowe");
		lblpoleObowizkowe.setForeground(Color.RED);
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_contentPane.createSequentialGroup()
							.addComponent(labelNazwisko, GroupLayout.PREFERRED_SIZE, 76, GroupLayout.PREFERRED_SIZE)
							.addContainerGap(348, Short.MAX_VALUE))
						.addGroup(gl_contentPane.createSequentialGroup()
							.addComponent(labelNazwaFirmy, GroupLayout.PREFERRED_SIZE, 94, GroupLayout.PREFERRED_SIZE)
							.addContainerGap(330, Short.MAX_VALUE))
						.addGroup(gl_contentPane.createSequentialGroup()
							.addComponent(labelAdres, GroupLayout.PREFERRED_SIZE, 94, GroupLayout.PREFERRED_SIZE)
							.addContainerGap(330, Short.MAX_VALUE))
						.addGroup(gl_contentPane.createSequentialGroup()
							.addComponent(labelMiejscowosc, GroupLayout.PREFERRED_SIZE, 94, GroupLayout.PREFERRED_SIZE)
							.addContainerGap(330, Short.MAX_VALUE))
						.addGroup(gl_contentPane.createSequentialGroup()
							.addComponent(labelNIP, GroupLayout.PREFERRED_SIZE, 94, GroupLayout.PREFERRED_SIZE)
							.addContainerGap(330, Short.MAX_VALUE))
						.addGroup(gl_contentPane.createSequentialGroup()
							.addComponent(labelTelefon, GroupLayout.PREFERRED_SIZE, 94, GroupLayout.PREFERRED_SIZE)
							.addContainerGap(330, Short.MAX_VALUE))
						.addGroup(gl_contentPane.createSequentialGroup()
							.addComponent(labelE_mail, GroupLayout.PREFERRED_SIZE, 94, GroupLayout.PREFERRED_SIZE)
							.addContainerGap(330, Short.MAX_VALUE))
						.addGroup(gl_contentPane.createSequentialGroup()
							.addGroup(gl_contentPane.createParallelGroup(Alignment.TRAILING)
								.addComponent(labelEdycjaKlienta, GroupLayout.DEFAULT_SIZE, 411, Short.MAX_VALUE)
								.addGroup(gl_contentPane.createSequentialGroup()
									.addComponent(labelImie, GroupLayout.PREFERRED_SIZE, 66, GroupLayout.PREFERRED_SIZE)
									.addPreferredGap(ComponentPlacement.RELATED, 65, Short.MAX_VALUE)
									.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
										.addComponent(textFieldNazwisko, GroupLayout.PREFERRED_SIZE, 280, GroupLayout.PREFERRED_SIZE)
										.addComponent(textFieldImie, GroupLayout.PREFERRED_SIZE, 280, GroupLayout.PREFERRED_SIZE)
										.addComponent(textFieldNazwaFirmy, GroupLayout.PREFERRED_SIZE, 280, GroupLayout.PREFERRED_SIZE)
										.addComponent(textFieldAdres, GroupLayout.PREFERRED_SIZE, 280, GroupLayout.PREFERRED_SIZE)
										.addComponent(textFieldMiejscowosc, GroupLayout.PREFERRED_SIZE, 280, GroupLayout.PREFERRED_SIZE)
										.addComponent(textFieldKod_pocztowy, GroupLayout.PREFERRED_SIZE, 280, GroupLayout.PREFERRED_SIZE)
										.addComponent(textFieldNIP, GroupLayout.PREFERRED_SIZE, 280, GroupLayout.PREFERRED_SIZE)
										.addComponent(textFieldTelefon, GroupLayout.PREFERRED_SIZE, 280, GroupLayout.PREFERRED_SIZE)
										.addComponent(textFieldE_mail, GroupLayout.PREFERRED_SIZE, 280, GroupLayout.PREFERRED_SIZE)
										.addComponent(lblpoleObowizkowe)
										.addComponent(buttonPotwierdz, GroupLayout.PREFERRED_SIZE, 128, GroupLayout.PREFERRED_SIZE))))
							.addGap(13))
						.addGroup(gl_contentPane.createSequentialGroup()
							.addComponent(labelKod_poczotwy, GroupLayout.PREFERRED_SIZE, 111, GroupLayout.PREFERRED_SIZE)
							.addContainerGap())))
		);
		gl_contentPane.setVerticalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addContainerGap()
					.addComponent(labelEdycjaKlienta)
					.addGap(22)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(labelImie)
						.addComponent(textFieldImie, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(18)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(labelNazwisko, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
						.addComponent(textFieldNazwisko, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(18)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(labelNazwaFirmy, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
						.addComponent(textFieldNazwaFirmy, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(18)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(labelAdres, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
						.addComponent(textFieldAdres, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(18)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(labelMiejscowosc, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
						.addComponent(textFieldMiejscowosc, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(18)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(labelKod_poczotwy, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
						.addComponent(textFieldKod_pocztowy, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(18)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(labelNIP, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
						.addComponent(textFieldNIP, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(18)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(labelTelefon, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
						.addComponent(textFieldTelefon, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(18)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(labelE_mail, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
						.addComponent(textFieldE_mail, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(lblpoleObowizkowe)
					.addPreferredGap(ComponentPlacement.RELATED, 33, Short.MAX_VALUE)
					.addComponent(buttonPotwierdz, GroupLayout.PREFERRED_SIZE, 31, GroupLayout.PREFERRED_SIZE)
					.addContainerGap())
		);
		contentPane.setLayout(gl_contentPane);
		setPola();
	}
	
	private void setPola(){
		int row = MenuKlient.tableKlienci.getSelectedRow();
		JTable tempTable = MenuKlient.tableKlienci;
			textFieldImie.setText(tempTable.getValueAt(row, 4).toString());
			textFieldNazwisko.setText(tempTable.getValueAt(row, 3).toString());
			textFieldNazwaFirmy.setText(tempTable.getValueAt(row, 1).toString());
			textFieldAdres.setText(tempTable.getValueAt(row, 5).toString());
			textFieldMiejscowosc.setText(tempTable.getValueAt(row, 6).toString());
			textFieldKod_pocztowy.setText(tempTable.getValueAt(row, 7).toString());
			textFieldNIP.setText(tempTable.getValueAt(row, 2).toString());
			textFieldTelefon.setText(tempTable.getValueAt(row, 8).toString());
			textFieldE_mail.setText(tempTable.getValueAt(row, 9).toString());
		
	}
}
