import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.NumberFormat;
import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

public class NowyProdukt extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField textFieldNazwa;
	private JFormattedTextField textFieldCenaNetto;
	private JFormattedTextField textFieldCenaBrutto;
	private NumberFormat amountFormat;
	private JComboBox<Double> comboBoxVat;
	private JLabel labelInformacja;
	private JLabel labelOstrzezenie = new JLabel("");
	private float CenaBrutto;
	private float CenaNetto;
	private String NazwaProduktu;
	private boolean SprawdzenieNazwy;

	/**
	 * Launch the application.
	 */
	public static void NewProductWindow() {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					NowyProdukt frame = new NowyProdukt();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public NowyProdukt() {
		setResizable(false);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 450, 384);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		
		JLabel labelNowyProdukt = new JLabel("Nowy Produkt");
		labelNowyProdukt.setHorizontalAlignment(SwingConstants.CENTER);
		labelNowyProdukt.setFont(new Font("Arial", Font.BOLD, 20));
		
		JLabel labelNazwa = new JLabel("Nazwa*");
		labelNazwa.setFont(new Font("Arial", Font.PLAIN, 15));
		
		JLabel labelCenaBnetto = new JLabel("Cena Netto*");
		labelCenaBnetto.setFont(new Font("Arial", Font.PLAIN, 15));
		
		JLabel labelKwotaVat = new JLabel("Vat (%)*");
		labelKwotaVat.setFont(new Font("Arial", Font.PLAIN, 15));
		
		JLabel labelCenaBrutto = new JLabel("Cena Brutto");
		labelCenaBrutto.setFont(new Font("Arial", Font.PLAIN, 15));
		
		textFieldNazwa = new JTextField();
		textFieldNazwa.setColumns(10);
		
		amountFormat = NumberFormat.getNumberInstance();
		textFieldCenaNetto = new JFormattedTextField(amountFormat);
		textFieldCenaNetto.setColumns(10);
				
		textFieldCenaBrutto = new JFormattedTextField(amountFormat);
		textFieldCenaBrutto.setEditable(false);
		textFieldCenaBrutto.setColumns(10);
		
		labelInformacja = new JLabel("*pole obowi\u0105zkowe");
		labelInformacja.setForeground(Color.RED);
		
		comboBoxVat = new JComboBox<Double>();
		comboBoxVat.setModel(new DefaultComboBoxModel<Double>(new Double[] {5.0,8.0,23.0}));
		
		JButton buttonAnuluj = new JButton("Anuluj");
		buttonAnuluj.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			dispose();}
		});
		buttonAnuluj.setFont(new Font("Tahoma", Font.PLAIN, 11));
		
		JButton buttonZapisz = new JButton("Zapisz");
		buttonZapisz.addActionListener(new ActionListener() { //przycisk dodania nowego produktu
			public void actionPerformed(ActionEvent arg0) {
				SprawdzenieNazwy = true; //zmienna okre�laj�ca czy pole z nazw� jest wype�nione
				NazwaProduktu = getNazwa(); //pobranie nazwy z pola
				int Vat = getVat(); //pobranie Vatu z pola
				try {
					setCenaNetto(); //pr�ba pobrania ceny netto
					if (SprawdzenieNazwy){
						if (CenaNetto>0)
						try {
							obliczCeneBrutto(); //pr�ba obliczenia ceny brutto na podstawie ceny netto i vatu
							try {
								Menu.baza.insertProdukt(NazwaProduktu, CenaNetto, Vat, CenaBrutto); //zapis do bazy
								dispose();
							} catch (Exception e) {
								labelOstrzezenie.setText("Nieprawid�owe dane wej�ciowe!");
							}
						} catch (Exception e) {
								labelOstrzezenie.setText("B��d oblicze�!");
						}
						else
							labelOstrzezenie.setText("Cena musi by� wi�ksza od zera!");
					}
				} catch (Exception e) {
					labelOstrzezenie.setText("Pole z cen� nie mo�e by� puste!");
				}
				
			}
		});
		
		buttonZapisz.setFont(new Font("Tahoma", Font.BOLD, 11));
		
		JButton buttonObliczCeneBrutto = new JButton("Oblicz cen\u0119 brutto");
		buttonObliczCeneBrutto.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					setCenaNetto();
					if (CenaNetto>0){
						try {
							obliczCeneBrutto();
							labelOstrzezenie.setText("Cena brutto obliczona poprawnie!");
						} catch (Exception e) {
							labelOstrzezenie.setText("B��d oblicze�!");
						}
					}
					else{
						textFieldCenaBrutto.setValue(new Double(0.0));
						labelOstrzezenie.setText("Cena musi by� wi�ksza od zera!");
					}
				} catch (Exception e) {
					labelOstrzezenie.setText("Pole z cen� nie mo�e by� puste!");
				}
			}
		});
		buttonObliczCeneBrutto.setFont(new Font("Tahoma", Font.PLAIN, 11));
		
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
			gl_contentPane.createParallelGroup(Alignment.TRAILING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addComponent(labelOstrzezenie, GroupLayout.DEFAULT_SIZE, 414, Short.MAX_VALUE)
						.addComponent(labelNowyProdukt, GroupLayout.PREFERRED_SIZE, 411, GroupLayout.PREFERRED_SIZE)
						.addGroup(gl_contentPane.createSequentialGroup()
							.addGroup(gl_contentPane.createParallelGroup(Alignment.TRAILING, false)
								.addComponent(labelNazwa, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
								.addComponent(labelCenaBnetto, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 97, Short.MAX_VALUE))
							.addPreferredGap(ComponentPlacement.RELATED, 37, Short.MAX_VALUE)
							.addGroup(gl_contentPane.createParallelGroup(Alignment.TRAILING)
								.addComponent(textFieldNazwa, GroupLayout.PREFERRED_SIZE, 280, GroupLayout.PREFERRED_SIZE)
								.addComponent(textFieldCenaNetto, GroupLayout.PREFERRED_SIZE, 280, GroupLayout.PREFERRED_SIZE)))
						.addGroup(gl_contentPane.createSequentialGroup()
							.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
								.addComponent(labelCenaBrutto, GroupLayout.PREFERRED_SIZE, 107, GroupLayout.PREFERRED_SIZE)
								.addComponent(buttonZapisz, GroupLayout.PREFERRED_SIZE, 128, GroupLayout.PREFERRED_SIZE)
								.addComponent(labelKwotaVat, GroupLayout.PREFERRED_SIZE, 66, GroupLayout.PREFERRED_SIZE)
								.addComponent(buttonObliczCeneBrutto, GroupLayout.PREFERRED_SIZE, 128, GroupLayout.PREFERRED_SIZE))
							.addPreferredGap(ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
							.addGroup(gl_contentPane.createParallelGroup(Alignment.TRAILING, false)
								.addComponent(buttonAnuluj, GroupLayout.PREFERRED_SIZE, 128, GroupLayout.PREFERRED_SIZE)
								.addComponent(textFieldCenaBrutto, GroupLayout.PREFERRED_SIZE, 280, GroupLayout.PREFERRED_SIZE)
								.addComponent(comboBoxVat, Alignment.LEADING, 0, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
								.addComponent(labelInformacja, Alignment.LEADING, GroupLayout.PREFERRED_SIZE, 129, GroupLayout.PREFERRED_SIZE))))
					.addContainerGap())
		);
		gl_contentPane.setVerticalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addContainerGap()
					.addComponent(labelNowyProdukt, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE)
					.addGap(18)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(labelNazwa, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
						.addComponent(textFieldNazwa, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(18)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.TRAILING)
						.addComponent(labelCenaBnetto, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
						.addComponent(textFieldCenaNetto, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(18)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(comboBoxVat, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(labelKwotaVat, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE))
					.addGap(18)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(labelCenaBrutto, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
						.addComponent(textFieldCenaBrutto, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(18)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(labelInformacja)
						.addComponent(buttonObliczCeneBrutto, GroupLayout.PREFERRED_SIZE, 31, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.RELATED, 30, Short.MAX_VALUE)
					.addComponent(labelOstrzezenie, GroupLayout.PREFERRED_SIZE, 27, GroupLayout.PREFERRED_SIZE)
					.addGap(27)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(buttonAnuluj, GroupLayout.PREFERRED_SIZE, 31, GroupLayout.PREFERRED_SIZE)
						.addComponent(buttonZapisz, GroupLayout.PREFERRED_SIZE, 31, GroupLayout.PREFERRED_SIZE))
					.addContainerGap())
		);
		contentPane.setLayout(gl_contentPane);
	}
	
	private void obliczCeneBrutto(){
		try {
			CenaNetto = (float) ((Number) textFieldCenaNetto.getValue()).doubleValue();
			double vat =  (double) comboBoxVat.getSelectedItem();
			double cena = CenaNetto + CenaNetto * (vat / 100);
			CenaBrutto = (float) cena;
			textFieldCenaBrutto.setValue(CenaBrutto);
		} catch (Exception e) {
			labelOstrzezenie.setText("Pole z cen� nie mo�e by� puste!");
		}
		
	}
	
	private int getVat(){
		double temp = (double) comboBoxVat.getSelectedItem();
		int Vat = (int) temp;
		return Vat;
	}
	
	private String getNazwa(){
		if (textFieldNazwa.getText().isEmpty()){ //sprawdzenie czy pole z nazw� jest wype�nione
			SprawdzenieNazwy = false;
			labelOstrzezenie.setText("Nazwa produktu jest wymagana.");
		}
		return textFieldNazwa.getText();
	}
		
	private void setCenaNetto(){
		CenaNetto = ((Number) textFieldCenaNetto.getValue()).floatValue();
	}
}
