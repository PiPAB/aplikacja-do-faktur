import java.awt.Component;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.border.EmptyBorder;

import db.Faktura;

public class Menu extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private static Menu frame;
	static Faktura baza = new Faktura();
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					frame = new Menu();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Menu() {
		setTitle("Program do faktur");
		setResizable(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 350);
		setMinimumSize(new Dimension(195, 320));
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		
		JButton ExitButton = new JButton("Exit");
		
		JButton KlienciButton = new JButton("Klienci");
		KlienciButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				MenuKlient.clientWindow();
				
			}
		});
		KlienciButton.setFont(new Font("Arial", Font.BOLD, 20));
		
		JButton TowaryButton = new JButton("Towary");
		TowaryButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				MenuProdukt.productWindow();
			}
		});
		TowaryButton.setFont(new Font("Arial", Font.BOLD, 20));
		TowaryButton.setAlignmentX(Component.CENTER_ALIGNMENT);
		
		JButton FakturyButton = new JButton("Faktury");
		FakturyButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				MenuFaktura.InvoiceWindow();
			}
		});
		FakturyButton.setFont(new Font("Arial", Font.BOLD, 20));
		FakturyButton.setAlignmentX(Component.CENTER_ALIGNMENT);
		
		JButton btnZamwienie = new JButton("Zam\u00F3wienie");
		btnZamwienie.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				MenuZamowienie.OrderWindow();
				
			}
		});
		btnZamwienie.setFont(new Font("Arial", Font.BOLD, 20));
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addComponent(ExitButton, GroupLayout.DEFAULT_SIZE, 404, Short.MAX_VALUE)
						.addGroup(gl_contentPane.createParallelGroup(Alignment.TRAILING, false)
							.addComponent(FakturyButton, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
							.addComponent(TowaryButton, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
							.addComponent(KlienciButton, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
							.addComponent(btnZamwienie, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
					.addContainerGap())
		);
		gl_contentPane.setVerticalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGap(20)
					.addComponent(FakturyButton)
					.addGap(20)
					.addComponent(TowaryButton)
					.addGap(20)
					.addComponent(KlienciButton)
					.addGap(18)
					.addComponent(btnZamwienie, GroupLayout.PREFERRED_SIZE, 33, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED, 58, Short.MAX_VALUE)
					.addComponent(ExitButton)
					.addContainerGap())
		);
		contentPane.setLayout(gl_contentPane);
		
		ExitButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				baza.closeConnection();
				System.exit(0);;}
		});
	}

}
