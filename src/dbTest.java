import java.util.List;
 
import db.Klient;
import db.Produkt;
import db.Faktura;
import db.Zamowienie;
 
public class dbTest {
 
    public static void main(String[] args) {
        Faktura b = new Faktura();
        
        // WSTAWIENIE KLIENTA == insertKlient(String NazwaFirmy, String NIP, String Imie, String Nazwisko, String Adres, String KodPocztowy, String Miejscowosc, String Telefon, String Email)
        //b.insertKlient("Nazwa Firmy 1", "123-123-12-12", "Jan", "Kowalski", "ul. Wroclawska", "12-121", "Wroclaw", "71 711 11 11", "test@test.pl");
        //b.insertKlient("Nazwa Firmy 2", "123-123-12-11", "Janek", "Kowalski", "ul. Swidnicka", "12-122", "Swidnica", "71 711 22 22", "test2@test2.pl");
        
        // USUNIECIE KLIENTA == deleteKlient(int idKlient)
        //b.deleteKlient(1);
        
        // AKTUALIZACJA KLIENTA == updateKlient(int IdKlient, String NazwaFirmy, String NIP, String Imie, String Nazwisko, String Adres, String KodPocztowy, String Miejscowosc, String Telefon, String Email)
        //b.updateKlient(1, "Nowa Nazwa Firmy", "123-123-12-12", "Jan", "Kowalski", "ul. Wroclawska", "12-121", "Wroclaw", "71 711 11 11", "test@test.pl");

        
        // WSTAWIENIE PRODUKTU == insertProdukt(String NazwaProduktu, float CenaNetto, int Vat, float CenaBrutto)
        // ew. przy wstawianiu CenaBrutto = CenaNetto * Vat
        //b.insertProdukt("Produkt 1", 100, 23, 123);
        //b.insertProdukt("Produkt 2", 200, 23, 246);
        //b.insertProdukt("Produkt 3", 300, 23, 369);
        //b.insertProdukt("Produkt 4", 400, 23, 492);
        
        // USUNIECIE PRODUKTU == deleteProdukt(int idProdukt)
        //b.deleteProdukt(8);
        
        // AKTUALIZACJA PRODUKTU == updateProdukt(int idProdukt, String NazwaProduktu, float CenaNetto, int Vat, float CenaBrutto)
        //b.updateProdukt(2, "Produkt 2 nowy", 201, 24, 247);
        
        
        // WSTAWIENIE ZAMOWIENIA == insertZamowienie(int NrZamowienia, int Ilosc, String DataZamowienia, int FkIdKlient, int FkIdProdukt)
        //b.insertZamowienie(3, 40, "2016-12-25", 1, 3);
        //b.insertZamowienie(3, 50, "2016-12-25", 1, 4);
        //b.insertZamowienie(1, 10, "2016-12-15", 2, 2);        
        
        // USUNIECIE ZAMOWIENIA == deleteZamowienie(int NrZamowienia)
        //b.deleteZamowienie(1);
        
        // AKTUALIZACJA ZAMOWIENIA == updateZamowienie(int NrZamowienia, int Ilosc, String DataZamowienia, int FkIdKlient, int FkIdProdukt)
        //b.updateZamowienie(2, 2, 11, "2016-12-16", 1, 2);
        

        List<Klient> klienci = b.selectKlienci();
        List<Produkt> produkty = b.selectProdukty();
        List<Zamowienie> zamowienia = b.selectZamowienia();
 
        System.out.println("Lista klientow:\n");
        for(Klient i: klienci)
            System.out.println(i);
 
        System.out.println("\nLista produktow:\n");
        for(Produkt j: produkty)
            System.out.println(j);
        
        System.out.println("\nLista zamowien:\n");
        for(Zamowienie k: zamowienia)
           System.out.println(k);
 
        b.closeConnection();
    }
}