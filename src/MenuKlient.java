import java.awt.Color;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.EmptyBorder;

import db.Klient;
import javax.swing.table.DefaultTableModel;
import javax.swing.ListSelectionModel;
import javax.swing.border.BevelBorder;
import javax.swing.ImageIcon;

public class MenuKlient extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private static MenuKlient frameKlient;
	public static JTable tableKlienci;
	//private String[] naglowki = new String[]{"NazwaFirmy","NIP","Imie","Nazwisko","Adres","KodPocztowy","Miejscowosc","Telefon","Email"};
	//private static String[][] dane = new String[][]{{}};

	/**
	 * Launch the application.
	 */
	public static void clientWindow() {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					frameKlient = new MenuKlient();
					frameKlient.setVisible(true);
					
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	@SuppressWarnings("serial")
	public MenuKlient() {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 800, 600);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		
		JButton btnNowyKlient = new JButton("Nowy Klient");
		btnNowyKlient.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				NowyKlient.NewClientWindow();
			}
		});
		
		JButton btnEdycjaKlienta = new JButton("Edycja Klienta");
		btnEdycjaKlienta.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (tableKlienci.getSelectedRow()!=-1){
					EdycjaKlienta.EditClientWindow();
				}
			}
		});
		
		JButton btnPodgldKlienta = new JButton("Podgl\u0105d Klienta");
		btnPodgldKlienta.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (tableKlienci.getSelectedRow()!=-1){
					PodgladKlienta.ShowClientWindow();
				}
			}
		});
		
		JButton button = new JButton("Exit");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		button.setBackground(new Color(255, 69, 0));
		
		tableKlienci = new JTable();
		tableKlienci.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
		tableKlienci.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		tableKlienci.setModel(new DefaultTableModel(
			new Object[][] {
			},
			new String[] {
				"Id Klienta", "Nazwa Firmy", "NIP", "Nazwisko", "Imi\u0119", "Adres", "Miejscowo\u015B\u0107", "Kod Pocztowy", "Telefon", "Email"
			}
		) {
			boolean[] columnEditables = new boolean[] {
				false, false, false, false, false, false, false, false, false, false
			};
			public boolean isCellEditable(int row, int column) {
				return columnEditables[column];
			}
		});
		tableKlienci.setAutoCreateRowSorter(true);
		JScrollPane scrollPane = new JScrollPane(tableKlienci);
		
		JButton buttonOdwiez = new JButton("");
		buttonOdwiez.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				zaczytajDane();
			}
		});
		buttonOdwiez.setIcon(new ImageIcon(MenuKlient.class.getResource("/com/sun/javafx/scene/web/skin/Redo_16x16_JFX.png")));
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addComponent(buttonOdwiez, Alignment.TRAILING)
						.addGroup(Alignment.TRAILING, gl_contentPane.createParallelGroup(Alignment.LEADING)
							.addComponent(btnNowyKlient, GroupLayout.PREFERRED_SIZE, 140, GroupLayout.PREFERRED_SIZE)
							.addComponent(btnEdycjaKlienta, GroupLayout.PREFERRED_SIZE, 140, GroupLayout.PREFERRED_SIZE)
							.addComponent(btnPodgldKlienta, GroupLayout.PREFERRED_SIZE, 140, GroupLayout.PREFERRED_SIZE)))
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_contentPane.createSequentialGroup()
							.addGap(18)
							.addComponent(scrollPane, GroupLayout.DEFAULT_SIZE, 596, Short.MAX_VALUE))
						.addGroup(Alignment.TRAILING, gl_contentPane.createSequentialGroup()
							.addGap(516)
							.addComponent(button, GroupLayout.PREFERRED_SIZE, 98, GroupLayout.PREFERRED_SIZE)))
					.addContainerGap())
		);
		gl_contentPane.setVerticalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_contentPane.createSequentialGroup()
							.addContainerGap()
							.addComponent(btnNowyKlient, GroupLayout.PREFERRED_SIZE, 48, GroupLayout.PREFERRED_SIZE)
							.addGap(18)
							.addComponent(btnEdycjaKlienta, GroupLayout.PREFERRED_SIZE, 48, GroupLayout.PREFERRED_SIZE)
							.addGap(18)
							.addComponent(btnPodgldKlienta, GroupLayout.PREFERRED_SIZE, 48, GroupLayout.PREFERRED_SIZE))
						.addGroup(gl_contentPane.createSequentialGroup()
							.addGap(12)
							.addComponent(scrollPane, GroupLayout.DEFAULT_SIZE, 476, Short.MAX_VALUE)))
					.addGap(18)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addComponent(button, GroupLayout.PREFERRED_SIZE, 34, GroupLayout.PREFERRED_SIZE)
						.addComponent(buttonOdwiez))
					.addContainerGap())
		);
		contentPane.setLayout(gl_contentPane);
		zaczytajDane();
	}
	
	static void zaczytajDane(){
		List<Klient> klienci = Menu.baza.selectKlienci();
		DefaultTableModel tableModel = (DefaultTableModel) tableKlienci.getModel();
		tableModel.setRowCount(0);
		String[] TempDane = new String[10];
		for(Klient i:klienci){
			TempDane[0] = Integer.toString(i.getIdKlient());
			TempDane[1] = i.getNazwaFirmy();
			TempDane[2] = i.getNIP();
			TempDane[3] = i.getNazwisko();
			TempDane[4] = i.getImie();
			TempDane[5] = i.getAdres();
			TempDane[6] = i.getMiejscowosc();
			TempDane[7] = i.getKodPocztowy();
			TempDane[8] = i.GetTelefon();
			TempDane[9] = i.getEmail();
			
			tableModel.addRow(TempDane);
		}
		tableKlienci.setModel(tableModel);
		tableKlienci.repaint();
		if (tableKlienci.getRowCount()>0){
			tableKlienci.changeSelection(0, 0, false, false);
		}
		
	}
}
