package db;
 
public class Produkt {
    private int idProdukt;
    private String NazwaProduktu;
    private float CenaNetto;
    private int Vat;
    private float CenaBrutto;

    public int getIdProdukt() {
        return idProdukt;
    }
    public String getNazwaProduktu() {
        return NazwaProduktu;
    }
    public void setNazwaProduktu(String NazwaProduktu) {
        this.NazwaProduktu = NazwaProduktu;
    }
    public float getCenaNetto() {
        return CenaNetto;
    }
    public void setCenaNetto(float CenaNetto) {
        this.CenaNetto = CenaNetto;
    }
    public int getVAT () {
    	return Vat;
    }
    public void setVat (int Vat) {
    	this.Vat = Vat;
    }
    public float getCenaBrutto() {
    	return CenaBrutto;
    }
    public void setCenaBrutto (float CenaBrutto) {
    	this.CenaBrutto = CenaBrutto;
    }
    
    public Produkt(int idProdukt, String NazwaProduktu, float CenaNetto, int Vat, float CenaBrutto) {
        this.idProdukt = idProdukt;
        this.NazwaProduktu = NazwaProduktu;
        this.CenaNetto = CenaNetto;
        this.Vat = Vat;
        this.CenaBrutto = CenaBrutto;       
    }
    
    @Override
    public String toString() {
        return idProdukt+" | " + NazwaProduktu + " | " + CenaNetto + " | " + Vat + " | " + CenaBrutto;
    }
    
}